class Comment < ActiveRecord::Base
	belongs_to :post
	validates_precense_of :post_id
	validates_precense_of :body
end
